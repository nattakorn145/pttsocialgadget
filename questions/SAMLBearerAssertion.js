
    // Gadget Calling SAP Jam API
    // 1. Request resources (OAuth 2.0  Service Name, OData request)
    // 7. Response (OData - JSON, XML)
    // 
    // gadgets.io.makeRequest Parameters
    // ---------------------------------
    // gadgets.io.makeRequest(url, callback, opt_params);
    // url: OData Call URL
    // callback: Callback function used to process the response.
    // opt_params: Additional OData proxy request parameters (shown below):
    //    AUTHORIZATION: The type of authentication to use when fetching the content.
    //    OAUTH_SERVICE_NAME: The nickname the gadget uses to refer to the OAuth <Service> element from its XML spec.
    //    CONTENT_TYPE: The type of content to retrieve at the specified URL.
    function loadGroups() {
      gadgets.io.makeRequest("https://developer.sapjam.com/api/v1/OData/Groups('Fs87CDOpeHVuFyStA2Ds1W')/AllDiscussions?$format=json",
        function(result) {
          console.log(result);

			if (result.data && result.data.d.results && result.data.d.results.length )
			{	
				var htmlContent = '';
				$.each( result.data.d.results, function( key, value ) {
  					console.log( 'key-'+key + ": " + value.Name + " - " +value.id );
					htmlContent = htmlContent+'<br/><a href="https://developer.sapjam.com'+'/discussions/'+value.id+'" target="_parent">'+value.Name + '</a><br/>' ;
				});
	          		$("#all-questions").html(htmlContent);
			}
        },
        {
          AUTHORIZATION: 'OAUTH2',
          OAUTH_SERVICE_NAME: 'socialgadget',
          CONTENT_TYPE: gadgets.io.ContentType.JSON
        });
    }

  gadgets.util.registerOnLoadHandler(function() {
	loadGroups();
  });
