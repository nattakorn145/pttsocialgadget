/**
 * STEP 2 (Implementing the "Add Topic" button)
 * - Encode object as JSON string -> toJSON(obj)
 * - Decode JSON string into an object -> toObject(str)
 * - Stores data in the OpenSocial gadget -> addInput()
 */

/** Encode object as JSON string */
function toJSON(obj) { 
	return gadgets.json.stringify(obj); 
}

/** Decode JSON string into an object */
function toObject(str) {
    return gadgets.json.parse(str);
}

/** Stores data in the OpenSocial gadget */
function addInput(){
	// Getting the state
	var state = wave.getState();
	
	// Retrieves topics from storage.
	//var jsonString = state.get('topics','[]');
	var jsonString = '[]';
	
	// Converts JSON to an array of topics
	var topics = toObject(jsonString);
	
	// Push textbox value into the array and set the textbox to blank
	topics.push(document.getElementById('textBox').value);
	document.getElementById('textBox').value = '';
		
	// Submit everything to storage
	state.submitDelta({'topics' : toJSON(topics)});
}

/** 
 * STEP 3 (Rendering topics)
 * - Get state
 * - Retrieve topics
 * - Add topics to the canvas
 * - Create "Add topic" button to the footer
 * - Adjust window size dynamically
 */

// Renders the gadget
function renderInfo() {
    /** Get state */
    if (!wave.getState()) {
        return;
    }
    var state = wave.getState();

   /** check permission**/
/*
  osapi.people.getViewer().execute(function(data) {
      	var osapiOutput = '';
        osapiOutput += "<input type='hidden' id='custom_html_widget_viewer' value='" + data.id + "' />";
	var temp = JSON.stringify(data); 
	osapiOutput += "===Viewer====" + temp + "=======";
        document.body.insertAdjacentHTML("beforeend", osapiOutput);
  }); 
*/

/*
  osapi.people.getGadgetData().execute(function(dataForID) {  
	var osapiOutput = '';
        //osapiOutput += "<input type='hidden' id='custom_html_widget_viewer' value='" + dataForID + "' />";
	var temp = JSON.stringify(dataForID); 
	osapiOutput += "===Viewer====" + temp + "=======";
	osapiOutput += "=== dataForID ====" + temp + "=======";
        document.body.insertAdjacentHTML("beforeend", osapiOutput);
  }); 
*/

osapi.appdata.get({userId: '@viewer', groupId: '@self', keys: ['stuff']}).execute(function(userData) {
	var osapiOutput = '';
        osapiOutput += "<br>osapi.appdata.get: " + gadgets.json.stringify(userData) + ".";
	document.body.insertAdjacentHTML("beforeend", osapiOutput);
});


   /** check permission**/
/*
  osapi.people.getOwner().execute(function(data) {


 	if (! current_user_id.error) {
              var viewerDiv = document.getElementById(‘current_user_id’);
              viewerDiv.innerHTML = data.displayName;
        };

       var osapiOutput = '';
       osapiOutput += "<input type='hidden' id='custom_html_widget_owner' value='" + data.id + "' />";
	console.log(data);
       osapiOutput += "==== Owner ===" + data.id + "=======";

       document.body.insertAdjacentHTML("beforeend", osapiOutput);
    });

    var owner_id = document.getElementById('custom_html_widget_owner'); 
    var viewer_id = document.getElementById('custom_html_widget_viewer'); 
    var isOwner = false;
    if (owner_id==viewer_id)
    {
	isOwner = true;
    }
 */






var isOwner = true;
   
    /** Retrieve topics */
    var topics = toObject(state.get('topics','[]'));
    /** Add topics to the canvas */
    var html = "";
    if (topics.length>0){
        html += '<div class="custom-html">' + topics[0] + '</div>';
    }
    
    /** Create "Add Content" button to the footer */
    if (isOwner)
    {
	var temp = '';
	if (topics.length>0){
		temp = topics[0];
	}        
        html += '<hr/><br/><textarea id="textBox">'+temp+'</textarea><button id="addInput" onclick="addInput()">Update Custom HTML</button>';
     }
     else
     {
        html += '<style>#header{display:none;}</style>';
     }

     document.getElementById('footer').innerHTML = html;
     
    /** Adjust window size dynamically */
    gadgets.window.adjustHeight();
}

// Initializes gadget, sets callbacks
function init() {
    if (wave && wave.isInWaveContainer()) {
    	// Loads the gadget's initial state and the subsequent changes to it
        wave.setStateCallback(renderInfo);
        
        // Loads participants and any changes to them
        wave.setParticipantCallback(renderInfo);
    }
}

// Initializes gadget after receiving a notification that the page is loaded and the DOM is ready.
gadgets.util.registerOnLoadHandler(init);

